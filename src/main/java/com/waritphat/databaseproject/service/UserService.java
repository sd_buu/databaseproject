/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.databaseproject.service;

import com.waritphat.databaseproject.dao.UserDao;
import com.waritphat.databaseproject.model.User;

/**
 *
 * @author domem
 */
public class UserService {
    public User login(String name, String password){
        UserDao userDao = new UserDao();
        User user = userDao.getByName(name);
        if (user.getPassword().equals(password)){
            return user;
        }
        return null;
    }
}
