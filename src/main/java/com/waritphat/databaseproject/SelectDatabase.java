/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author domem
 */
public class SelectDatabase {
    
    public static void main(String[] args) {
        //Connect Database
        Connection conn = null;
        String url = "jdbc:sqlite:test_d_coffee.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connect to SQLite  has benn establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Selection
        String sql = "SELECT * FROM category";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                System.out.println(rs.getInt("category_id") + " " + rs.getString("category_name"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //Close Database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
