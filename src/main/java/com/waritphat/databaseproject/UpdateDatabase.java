/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author domem
 */
public class UpdateDatabase {

    public static void main(String[] args) {
        //Connect Database
        Connection conn = null;
        String url = "jdbc:sqlite:test_d_coffee.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connect to SQLite  has benn establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Update
        String sql = "UPDATE category SET category_name=? WHERE category_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "MyCoffee");
            stmt.setInt(2, 1);
            int status = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //Close Database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
